#!/usr/bin/env python3
import argparse
import connexion
from prometheus_client import generate_latest
from mobile_wallet import encoder

def main(args):
    app = connexion.App(__name__, specification_dir='./swagger/')
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api('swagger.yaml', arguments={'title': 'Money transfer API'}, pythonic_params=True, options={"swagger_ui": True})

    @app.route('/healthz')
    def health(**kwargs):
        """Return success, used for container readiness/liveness"""
        return 'OK', 200

    @app.route('/metrics')
    def metrics(**kwargs):
        """Provide current prometheus metrics"""
        return generate_latest().decode('utf-8')

    app.run(port=int(args.port))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port', default='8080', help='Port to listem on')
    args = parser.parse_args()
    print(args)
    main(args)
