# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from mobile_wallet.models.balance import Balance  # noqa: E501
from mobile_wallet.models.creds import Creds  # noqa: E501
from mobile_wallet.models.transaction import Transaction  # noqa: E501
from mobile_wallet.test import BaseTestCase


class TestUsersController(BaseTestCase):
    """UsersController integration test stubs"""

    def test_add_transaction(self):
        """Test case for add_transaction

        Transfer money to another user
        """
        body = Transaction()
        response = self.client.open(
            '/api/v1/transaction',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_balance(self):
        """Test case for get_balance

        get account balance
        """
        response = self.client.open(
            '/api/v1/balance',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_history(self):
        """Test case for get_history

        get history of transactions
        """
        response = self.client.open(
            '/api/v1/transaction',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_token(self):
        """Test case for get_token

        token authentication
        """
        body = {'user': 'testuser', 'password': 'testpassword'}
        response = self.client.open(
            '/api/v1/auth',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
