import six
from typing import List
from jose import JWTError, jwt
import  mobile_wallet.config as config
from werkzeug.exceptions import Unauthorized
"""
controller generated to handled auth operation described at:
https://connexion.readthedocs.io/en/latest/security.html
"""
def check_bearerAuth(token):
    try:
        return jwt.decode(token, config.JWT_SECRET, algorithms=[config.JWT_ALGORITHM])
    except JWTError as e:
        six.raise_from(Unauthorized, e)


