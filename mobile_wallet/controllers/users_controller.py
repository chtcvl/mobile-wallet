import connexion
import six
import os
import json
import datetime
import time

from prometheus_client import Counter, Gauge
from mobile_wallet.models.balance import Balance  # noqa: E501
from mobile_wallet.models.creds import Creds  # noqa: E501
from mobile_wallet.models.transaction import Transaction  # noqa: E501
from mobile_wallet import util

from jose import JWTError, jwt
import  mobile_wallet.config as config
from decimal import Decimal

c_ok = Counter('request_successful', 'Successful request count')

# TODO: add orm and database interface (not in cope of test assignment). Using json file for simple demonstration purpose

def add_transaction(body, user, token_info):  # noqa: E501
    """Transfer money to another user

    Add transaction to backend DB # noqa: E501

    :param body: Transaction to add
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = Transaction.from_dict(connexion.request.get_json())  # id, user_id, recipient, amount, date
    d = None
    with open(config.data, 'r') as f:
        d = json.load(f)
    hacky_id = int(datetime.datetime.now().timestamp())
    hacky_id += 1
    d['transactions'].append({'id': hacky_id, 'user_id': user, 'recipient': body.recipient, 'amount': body.amount, 'date': str(datetime.datetime.now())})
    hacky_id += 1
    d['transactions'].append({'id': hacky_id, 'user_id': 0, 'recipient': user, 'amount': ''.join(['-',body.amount]), 'date': str(datetime.datetime.now())})
    with open(config.data, 'w') as of:
        json.dump(d, of)
    c_ok.inc()
    return 'Item Created', 201


def get_balance(user, token_info):  # noqa: E501
    """get account balance

    Calling this endpoint returns current balance for an authenticated user # noqa: E501


    :rtype: Balance
    """
    d = None
    with open(config.data, 'r') as f:
        d = json.load(f)
    c_ok.inc()
    return json.dumps({'user': user, 'balance': str(sum([Decimal(i['amount']) for i in d['transactions'] if i['recipient'] == user]))}), 200


def get_history(user, token_info):  # noqa: E501
    """get history of transactions

    Retrieve a list of transactions made with the user`s account # noqa: E501

    :rtype: List[Transaction]
    """
    d = None
    with open(config.data, 'r') as f:
        d = json.load(f)
    c_ok.inc()
    return json.dumps([i for i in d['transactions'] if ((i['user_id'] == user) or (i['recipient'] == user))]), 200


def _current_timestamp() -> int:
    return int(time.time())


def get_token(body):  # noqa: E501
    """token authentication

    Returns JWT if credentials are correct. Switch to OAuth2 when used in production # noqa: E501

    :param body: 
    :type body: dict | bytes

    :rtype: str
    """
    if connexion.request.is_json:
        body = Creds.from_dict(connexion.request.get_json())  # noqa: E501
    # TODO: check creds if decide to go with basic auth
    timestamp = _current_timestamp()
    payload = {
        "iss": config.JWT_ISSUER,
        "iat": int(timestamp),
        "exp": int(timestamp + config.JWT_LIFETIME_SECONDS),
        "sub": str(body.username),
    }
    c_ok.inc()
    return jwt.encode(payload, config.JWT_SECRET, algorithm=config.JWT_ALGORITHM)

